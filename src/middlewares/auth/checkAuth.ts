import {IKoaContext} from '@interfaces/index'
import { Next } from 'koa'
import { authServiceFactory } from '@services/auth/index'

export default async (ctx: IKoaContext, next: Next) => {
    let token: string

    if(ctx.request.headers && ctx.request.headers['authorization']) {
        token = ctx.request.headers['authorization'].split('Bearer ')[1]
    }
    
    if(!token) return ctx.status = 403

    const verify = authServiceFactory().verifyAccessJWT(token)

    ctx.user = verify

    await next()
 }
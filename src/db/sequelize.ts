import { Sequelize } from 'sequelize-typescript';
import config from '@config/index';

const sequelize = new Sequelize(config.pg);

sequelize.authenticate().then(async () => {
  console.log('success connect to postgres db');
  // await sequelize.sync({});
  // await sequelize.sync({ force: true });
}).catch((error) => {
  console.error('Unable to connect to the database:', error);
});

export default sequelize;

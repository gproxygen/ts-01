import { Table, Column, Model, DataType, IsEmail, Unique, AllowNull,  Scopes, HasMany } from 'sequelize-typescript';
import { UserRole } from '@models/index';

@Scopes(() => ({
  withUserRole: {
    include: [UserRole],
  },
}))

@Table({
  timestamps: true,
})

class User extends Model<User> {

  @AllowNull(false)
  @Unique
  @IsEmail
  @Column(DataType.STRING)
  email: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  password: string;

  @HasMany(() => UserRole, 'userId')
  roles: UserRole;  
}

export default User;
import { Table, Column, Model, DataType, AllowNull, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { User, RoleName, RoleAction } from '@models/index';

@Table({
  timestamps: false,
})
class UserRole extends Model<UserRole> {

  @ForeignKey(() => User)
  @AllowNull(false)
  @Column(DataType.INTEGER)
  userId: number;

  @ForeignKey(() => RoleName)  
  @Column(DataType.INTEGER)
  roleNameId: number;

  @ForeignKey(() => RoleAction)
  @Column(DataType.INTEGER)
  roleActionId: number;

  @BelongsTo(() => User, 'userId')
  user: User;  

  @BelongsTo(() => RoleName, 'roleNameId')
  roleName: RoleName;    

  @BelongsTo(() => RoleAction, 'roleActionId')
  roleAction: RoleAction;  
}

export default UserRole;

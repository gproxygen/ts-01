import { Table, Column, Model, DataType, AllowNull, HasMany, Scopes, Unique} from 'sequelize-typescript';
import {RoleNameAction} from '@models/index';

@Scopes(() => ({
  withRoleNameAction: {
    include: [RoleNameAction],
  },
}))
@Table({
  timestamps: true,
})

class RoleName extends Model<RoleName> {

  @Unique
  @AllowNull(false)
  @Column(DataType.STRING)
  name: string;

  @HasMany(() => RoleNameAction, 'roleNameId')
  roleNameAction: RoleNameAction;  
}

export default RoleName;

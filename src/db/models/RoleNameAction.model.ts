import { Table, Column, Model, DataType, ForeignKey, BelongsTo, AllowNull, Scopes, PrimaryKey } from 'sequelize-typescript';
import {RoleName, RoleAction} from '@models/index'

@Scopes(() => ({
  withRole: {
    include: [RoleName],
  },
}))
@Table({
  timestamps: true,
})
class RoleNameAction extends Model<RoleNameAction> {

  @ForeignKey(() => RoleName)  
  @AllowNull(false)
  @Column(DataType.INTEGER)
  roleNameId: number;

  @ForeignKey(() => RoleAction)
  @AllowNull(false)
  @Column(DataType.INTEGER)
  roleActionId: number;

  @BelongsTo(() => RoleName, 'roleNameId')
  roleName: RoleName;    

  @BelongsTo(() => RoleAction, 'roleActionId')
  roleAction: RoleAction;  

}

export default RoleNameAction;

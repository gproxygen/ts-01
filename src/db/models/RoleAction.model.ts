import { Table, Column, Model, DataType, Default, AllowNull, Unique } from 'sequelize-typescript';

@Table({
  timestamps: true,
})
class RoleAction extends Model<RoleAction> {

  @Unique
  @AllowNull(false)
  @Column(DataType.STRING)
  key: string;

  @AllowNull(false)  
  @Column(DataType.STRING)
  title: string;

  @Default(0)
  @AllowNull(false)  
  @Column(DataType.INTEGER)
  parent: number;  

}

export default RoleAction;

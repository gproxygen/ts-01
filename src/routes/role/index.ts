import Router from 'koa-router';
import { createAction, createName } from './controllers';
import { checkAuth } from '@middlewares/auth/index';

const router = new Router();

router.prefix('/role');

router.post(
  '/name',
  // checkAuth,
  createName.bind(this),
);

router.post(
  '/action',
  // checkAuth,
  createAction.bind(this),
);

export default router;

import { IKoaContext } from '@interfaces/index';
import { IRoleNameBody } from '@services/domain/role/RoleNameService';
import {roleNameInstance} from '@services/domain/role/index'

export default async (ctx: IKoaContext) => {
  const body: IRoleNameBody = ctx.request.body;
  const data = await roleNameInstance.create(body);
  ctx.body = { data, success: true };  
};



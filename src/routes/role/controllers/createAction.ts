import { IKoaContext } from '@interfaces/index';
import { IRoleActionBody } from '@services/domain/role/RoleActionService';
import { roleActionInstance } from '@services/domain/role/index'

export default async (ctx: IKoaContext) => {
  const body: IRoleActionBody = ctx.request.body;
  const data = await roleActionInstance.create(body);
  ctx.body = { data, success: true };  
};



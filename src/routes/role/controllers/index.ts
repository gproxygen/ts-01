import  createName  from './createName';
import  createAction  from './createAction';

export {
  createName,
  createAction
};

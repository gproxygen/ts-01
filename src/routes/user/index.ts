import Router from 'koa-router';
import { me } from './controllers';
import { checkAuth } from '@middlewares/auth/index';

const router = new Router();

router.prefix('/auth');

router.post(
  '/me',
  checkAuth,
  me.bind(this),
);

export default router;

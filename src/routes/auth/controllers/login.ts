import { IKoaContext } from '@interfaces/index';
import { IRegisterBody } from '../interfaces/index';
import { authServiceFactory } from '@services/auth/index';

export const login = async (ctx: IKoaContext) => {
  const body: IRegisterBody = ctx.request.body;
  try {
    const data = await authServiceFactory().login(body);
    ctx.body = { data, success: true };
  } catch (error) {
    console.log(error);
    ctx.body = { success: false };
  }
};

import { IKoaContext } from '@interfaces/index';
import { IRegisterBody } from '../interfaces/index';
import { authServiceFactory } from '@services/auth/index';

export const register = async (ctx: IKoaContext) => {
  const body: IRegisterBody = ctx.request.body;
  try {
    const player = await authServiceFactory().register(body);
    ctx.body = { player, success: true };
  } catch (error) {
    console.log(error);
    ctx.body = { success: false };
  }
};

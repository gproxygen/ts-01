import { IKoaContext } from '@interfaces/index';
import { IRefreshBody } from '../interfaces/index';
import { authServiceFactory } from '@services/auth/index';

export const refresh = async (ctx: IKoaContext) => {
  const body: IRefreshBody = ctx.request.body;
  try {
    const data = await authServiceFactory().refreshToken(body.refreshToken);
    ctx.body = { data, success: true };
  } catch (error) {
    console.log(error);
    ctx.body = { success: false };
  }
};

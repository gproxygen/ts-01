import Router from 'koa-router';
import { login, register, refresh } from './controllers';

const router = new Router();

router.prefix('/auth');

router.post(
  '/login',
  login.bind(this),
);

router.post(
  '/register',
  register.bind(this),
);

router.post(
  '/refresh',
  refresh.bind(this),
);

export default router;

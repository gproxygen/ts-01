export interface IRegisterBody {
  email: string;
  password: string;
}

export interface ILoginBody {
  email: string;
  password: string;
}

export interface IRefreshBody {
  refreshToken: string;
}

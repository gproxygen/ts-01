import Router from 'koa-router';
import auth from './auth';
import role from './role';

const router = new Router();

router.get('/', (ctx: any) => {
  ctx.body = 'Hello';
});

router.use(auth.routes());
router.use(auth.allowedMethods());

router.use(role.routes());
router.use(role.allowedMethods());

export default router;

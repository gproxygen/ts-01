import { User } from '@models/index';
import { IRegisterBody, ILoginBody } from '@routes/auth/interfaces/index';
import { genSaltSync, hashSync, compareSync } from 'bcrypt';
import { sign, verify } from 'jsonwebtoken';

export default class AuthService {
  constructor () {}

  async register(body: IRegisterBody) {

    if (!body.email || !body.password) throw new Error('Неверные данные');

    const findUser = await User.findOne({ where: { email: body.email } });

    if (findUser) throw new Error('Пользователь с данным email уже зарегистрирован');

    const password = this.generatePassword(body.password);

    const createdUser = await User.create({ password, email: body.email });

    return createdUser;

  }

  async login(body: ILoginBody) {

    if (!body.email || !body.password) throw new Error('Неверные данные');

    const findUser = await User.findOne({ where: { email: body.email } });

    if (!findUser) throw new Error('Пользователь с данным email не зарегистрирован');

    if (!this.verifyPassword(findUser.password, body.password)) throw new Error('Введен неверный пароль');

    delete findUser.password;

    return this.generateJWT(findUser);

  }

  async refreshToken(refreshToken: string) {

    const refreshTokenVerify = this.verifyRefreshJWT(refreshToken);

    if (!refreshTokenVerify) throw new Error('Refresh token not valid');

    const findUser = await User.findOne({ where: { id: refreshTokenVerify.id } });

    return this.generateJWT(findUser);
  }

  async logout() {}

  generateJWT(owner: User) {

    if (!owner) return false;

    const accessToken = sign(owner.toJSON(), process.env.ACCESS_TOKEN_SECRET, {
      algorithm: 'HS256',
      expiresIn: process.env.ACCESS_TOKEN_LIFE,
    });

    const refreshToken = sign(owner.toJSON(), process.env.REFRESH_TOKEN_SECRET, {
      algorithm: 'HS256',
      expiresIn: process.env.REFRESH_TOKEN_LIFE,
    });

    return { accessToken, refreshToken };
  }

  verifyAccessJWT(token: string) {
    return verify(token, process.env.ACCESS_TOKEN_SECRET) as User;
  }

  verifyRefreshJWT(token: string) {
    return verify(token, process.env.REFRESH_TOKEN_SECRET) as User;
  }

  generatePassword(password: string) {
    const salt = genSaltSync(10);
    const hash = hashSync(password, salt);
    return hash;
  }

  verifyPassword(hashedPassword:string, inputPassword: string) {
    return compareSync(inputPassword, hashedPassword);
  }
}

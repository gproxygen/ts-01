import UserService from '@services/domain/user/UserService'

export interface ILoggerService {
  put(data: any): Promise<void>
}

export const userFactory = (loggerService: ILoggerService) => {
  return new UserService(loggerService)
}


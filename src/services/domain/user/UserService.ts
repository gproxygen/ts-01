import { User } from '@models/index';
import {ILoggerService} from '@services/domain/user/index'

export interface IUserService {
  getById(userId: number): string
  update(id: number): string
}

export default class UserService implements IUserService{
  constructor (private loggerService: ILoggerService) {}

  getById(userId: number) {
    this.loggerService.put({data: 'sd'})
    return 'string'
  }

  update(userId: number) {
    return 'string'
  }
}

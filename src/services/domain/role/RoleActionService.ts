import { RoleAction } from '@models/index';

export interface IRoleActionBody {
  key: string;
  title: string;
  parent: number;
}

export interface IRoleActionService {
  create(body: IRoleActionBody): Promise<IRoleActionBody>
}

export default class RoleActionService implements IRoleActionService{
  constructor () {}
  
  async create(body: IRoleActionBody){
    const newRoleAction = new RoleAction();
    newRoleAction.key = body.key;
    newRoleAction.title = body.title;
    newRoleAction.parent = body.parent || 0;
    await newRoleAction.save();
    return newRoleAction
  }

}

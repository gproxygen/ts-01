import { RoleName, RoleNameAction, RoleAction } from '@models/index';
import { IsArray } from 'sequelize-typescript';

export interface IRoleNameBody {
  name:string;
  roleActionIds? : [number];
}

export interface IRoleNameService {
  create(body: IRoleNameBody): Promise<RoleName>
}

export default class RoleNameService implements IRoleNameService{
  constructor () {}
  
  // async getById(roleNameId:number){
  //   const result = 
  // }

  async create(body: IRoleNameBody) {
   
    const arrayRoleName: any = []
    if(body.roleActionIds && Array.isArray(body.roleActionIds)){
      
      body.roleActionIds.forEach( async idAction => {
        const action = RoleAction.findByPk(idAction)
        if (action){
          arrayRoleName.push({ roleActionId: idAction })
        }      
      });
    }

   const result = await RoleName.create({
      name: body.name,
      roleNameAction: arrayRoleName,
     }, {
      include: [RoleNameAction],
     });
    return result
  }

}

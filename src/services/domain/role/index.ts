import RoleNameService from '@services/domain/role/RoleNameService'
import RoleActionService from '@services/domain/role/RoleActionService'

export const roleNameFactory = () => {
  return new RoleNameService()
}

export const roleActionFactory = () => {
  return new RoleActionService()
}


export const roleNameInstance = roleNameFactory()
export const roleActionInstance = roleActionFactory()


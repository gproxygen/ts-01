import { resolve } from 'path';
import { SequelizeOptions } from 'sequelize-typescript';

const host = 'http://localhost';
const port = 4000;

const config = {
  server: {
    host,
    port,
  },
  pg: {
    host: 'localhost',
    database: 'ts-01',
    dialect: 'postgres',
    username: 'postgres',
    password: 'postgres',
    storage: ':memory:',
    models: [resolve(`${process.cwd()}/src/db/models/**/*.model.ts`)],
    logging: false,
  } as SequelizeOptions,
};

export default config;

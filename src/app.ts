require('module-alias/register');
require('dotenv').config();

import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import cors from 'koa-cors';
import config from '@config/index';
import routes from '@routes/index';
import '@db/sequelize';

const app: Koa = new Koa();

app.use(cors());
app.use(bodyParser());

app.use(routes.routes()).use(routes.allowedMethods());

app.listen(config.server.port);
console.log('Server started and listening for:');
console.log(`http://localhost:${config.server.port}`);

export default app;

import * as koa from 'koa';

export interface IKoaContext extends koa.Context {}
